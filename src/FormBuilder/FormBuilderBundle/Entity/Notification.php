<?php

namespace FormBuilder\FormBuilderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Notification
 *
 * @ORM\Table(name="notification")
 * @ORM\Entity(repositoryClass="FormBuilder\FormBuilderBundle\Repository\NotificationRepository")
 */
class Notification
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="string", length=255)
     */
    private $content;

    /**
     * @ORM\ManyToOne(targetEntity="FormBuilder\FormBuilderBundle\Entity\Recipient")
     */
    private $recipient;

    /**
     * @ORM\ManyToOne(targetEntity="FormBuilder\FormBuilderBundle\Entity\Form", cascade={"remove"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $form;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Notification
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set recipient
     *
     * @param \FormBuilder\FormBuilderBundle\Entity\Recipient $recipient
     *
     * @return Notification
     */
    public function setRecipient(\FormBuilder\FormBuilderBundle\Entity\Recipient $recipient = null)
    {
        $this->recipient = $recipient;

        return $this;
    }

    /**
     * Get recipient
     *
     * @return \FormBuilder\FormBuilderBundle\Entity\Recipient
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * Set form
     *
     * @param \FormBuilder\FormBuilderBundle\Entity\Form $form
     *
     * @return Notification
     */
    public function setForm(\FormBuilder\FormBuilderBundle\Entity\Form $form = null)
    {
        $this->form = $form;

        return $this;
    }

    /**
     * Get form
     *
     * @return \FormBuilder\FormBuilderBundle\Entity\Form
     */
    public function getForm()
    {
        return $this->form;
    }
}
