<?php

namespace FormBuilder\FormBuilderBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('@FormBuilderFormBuilder/Default/index.html.twig');
    }
}
