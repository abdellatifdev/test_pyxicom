<?php
/**
 * Created by PhpStorm.
 * User: Unknown
 * Date: 28/10/2019
 * Time: 22:05
 */

namespace FormBuilder\FormBuilderBundle\Controller;
use FormBuilder\FormBuilderBundle\Entity\Recipient;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\Finder\Exception\AccessDeniedException;

class RecipientAdminController extends CRUDController
{
    public function createAction()
    {
        if (false === $this->admin->isGranted('CREATE')) {
            throw new AccessDeniedException();
        }
        $request = $this->getRequest();
        if ($request->isMethod('POST')) {
            $recipient = new Recipient();
            $recipient->setName($request->get('name'));
            $recipient->setEmail($request->get('email'));
            $this->admin->create($recipient);
            return $this->redirectToRoute('admin_formbuilder_formbuilder_recipient_list');
        }
        return $this->render("@FormBuilderFormBuilder/CRUD/Recipient/create.html.twig");
    }
}