<?php
/**
 * Created by PhpStorm.
 * User: Unknown
 * Date: 28/10/2019
 * Time: 11:11
 */

namespace FormBuilder\FormBuilderBundle\Controller;
use FormBuilder\FormBuilderBundle\Entity\Form;
use FormBuilder\FormBuilderBundle\Entity\Recipient;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class FormAdminController extends CRUDController
{
    public function createAction()
    {
        if (false === $this->admin->isGranted('CREATE')) {
            throw new AccessDeniedException();
        }
        $request = $this->getRequest();
        $recipientRepo = $this->getDoctrine()->getRepository(Recipient::class);
        $recipients = $recipientRepo->findAll();
        if ($request->isMethod('POST')) {
            $form = new Form();
            $form->setName($request->get('form_name'));
            $form->setData($request->get('fields'));
            if ($request->get('recipients')){
                foreach ($request->get('recipients') as $recipient){
                    $form->addRecipient($recipientRepo->find($recipient));
                }
            }
            $this->admin->create($form);
            return $this->redirectToRoute('admin_formbuilder_formbuilder_form_list');
        }
        return $this->render("@FormBuilderFormBuilder/CRUD/Form/create.html.twig",[
            'recipients' => $recipients
        ]);
    }

    public function editAction($id = null)
    {
        if (false === $this->admin->isGranted('EDIT')) {
            throw new AccessDeniedException();
        }
        $request = $this->getRequest();
        $id = $request->get($this->admin->getIdParameter());
        $existingObject = $this->admin->getObject($id);
        $recipientRepo = $this->getDoctrine()->getRepository(Recipient::class);
        $recipients = $recipientRepo->findAll();
        if ($request->isMethod('POST')) {
            $existingObject->setName($request->get('form_name'));
            $existingObject->setData($request->get('fields'));
            $existingObject->removeAllRecipients();
            if ($request->get('recipients')){
                foreach ($request->get('recipients') as $recipient){
                    $existingObject->addRecipient($recipientRepo->find($recipient));
                }
            }
            $this->admin->create($existingObject);
            return $this->redirectToRoute('admin_formbuilder_formbuilder_form_list');
        }

        return $this->render("@FormBuilderFormBuilder/CRUD/Form/edit.html.twig",[
            'form' => $existingObject,
            'recipients' => $recipients
        ]);
    }

    public function changeStatusAction($id)
    {
        $existingObject = $this->admin->getObject($id);
        $status = $existingObject->getStatus() ? false : true;
        $existingObject->setStatus($status);
        $this->admin->create($existingObject);
        return $this->redirectToRoute('admin_formbuilder_formbuilder_form_list');
    }

    public function cloneAction($id)
    {
        $existingObject = $this->admin->getObject($id);
        $recipientRepo = $this->getDoctrine()->getRepository(Recipient::class);
        $recipients = $recipientRepo->findAll();
        return $this->render("@FormBuilderFormBuilder/CRUD/Form/clone.html.twig",[
            'form' => $existingObject,
            'recipients' => $recipients
        ]);
    }

    public function deleteAction($id)
    {
        $existingObject = $this->admin->getObject($id);
        if ($existingObject === null){
            throw new NotFoundHttpException();
        }
        $existingObject->removeAllRecipients();
        $this->admin->delete($existingObject);
        return $this->redirectToRoute('admin_formbuilder_formbuilder_form_list');
    }
}