<?php
/**
 * Created by PhpStorm.
 * User: Unknown
 * Date: 24/10/2019
 * Time: 22:18
 */

namespace FormBuilder\FormBuilderBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class FormAdmin extends AbstractAdmin
{

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name',null,[
                'label' => 'Nom de formulaire',
                'type_options'       => array( 'delete' => false ),
            ])
            ->add('data', null, array(
                'template' => 'FormBuilderFormBuilderBundle:FormAdmin:data_field.html.twig',
                'label' => 'Nombre de champs'
            ))
            ->add('createdOn','datetime',array(
                'format' => 'd/m/Y',
                'label'  => 'Date de création'
            ))
            ->add('status', 'choice', [
                'editable' => false,
                'class' => 'Vendor\ExampleBundle\Entity\ExampleStatus',
                'choices' => [
                    0 => 'Brouillon',
                    1 => 'Publié',
                ],
            ])
            ->add('_action', null, [
                'actions' => [
                    'clone_change_status_delete' => [
                        'template' => 'FormBuilderFormBuilderBundle:FormAdmin:changeStatus.html.twig',
                    ],
                ]
                ])

            ;

    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->add('changeStatus', $this->getRouterIdParameter().'/change-status')
            ->add('clone', $this->getRouterIdParameter().'/clone');
    }
}