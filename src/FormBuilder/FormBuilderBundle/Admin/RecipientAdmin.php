<?php
/**
 * Created by PhpStorm.
 * User: Unknown
 * Date: 28/10/2019
 * Time: 22:02
 */

namespace FormBuilder\FormBuilderBundle\Admin;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class RecipientAdmin extends AbstractAdmin
{
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name',null,[
                'label' => 'Nom',
            ])
            ->add('email',null,[
                'label' => "Email"
            ])
            ;

    }
}