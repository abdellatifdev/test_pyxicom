<?php
/**
 * Created by PhpStorm.
 * User: Unknown
 * Date: 28/10/2019
 * Time: 23:24
 */

namespace FormBuilder\FormBuilderBundle\EventListener;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use FormBuilder\FormBuilderBundle\Entity\Form;
use FormBuilder\FormBuilderBundle\Entity\Notification;

class NotificationSubscriber implements EventSubscriber
{
    public function getSubscribedEvents()
    {
        return [
            Events::postPersist,
            Events::postUpdate,
        ];
    }
    public function postPersist(LifecycleEventArgs $args)
    {
        $this->index($args);
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->index($args);
    }

    public function index(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        if (!$entity instanceof Form) {
            return;
        }
        if ($entity->getRecipients()){
            $entityManager = $args->getObjectManager();
            foreach ($entity->getRecipients() as $recipient){
                $notification = new Notification();
                $notification->setContent('new notif');
                $notification->setRecipient($recipient);
                $notification->setForm($entity);
                $entityManager->persist($notification);
            }
            $entityManager->flush();
        }
    }
}