<?php
/**
 * Created by PhpStorm.
 * User: Unknown
 * Date: 28/10/2019
 * Time: 20:13
 */

namespace FormBuilder\FormBuilderBundle\Twig;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class JsonDecodeExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('json_decode', [$this, 'jsonDecode']),
        ];
    }

    public function jsonDecode($json)
    {
        return json_decode($json,true);
    }
}